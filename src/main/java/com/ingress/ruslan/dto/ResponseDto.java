package com.ingress.ruslan.dto;

import lombok.Data;

@Data
public class ResponseDto {

    private String msg;
    private CounterDto dto;

    public ResponseDto(String msg, CounterDto dto) {
        this.msg = msg;
        this.dto = dto;
    }
}
