package com.ingress.ruslan.service;

import com.ingress.ruslan.dto.CounterDto;

import java.util.List;

public interface CounterService {

    CounterDto getCounter(Long id);
    List<CounterDto> getCounters();
}
