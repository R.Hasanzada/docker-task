package com.ingress.ruslan.service;

import com.ingress.ruslan.domain.Counter;
import com.ingress.ruslan.dto.CounterDto;
import com.ingress.ruslan.repo.CounterRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Service
public class CounterServiceImpl implements CounterService{

    private final CounterRepo counterRepo;

    @Override
    public CounterDto getCounter(Long id) {
        Counter counter = counterRepo.findById(id)
                .orElseThrow(() -> new RuntimeException("Counter not found"));
        CounterDto counterDto = new CounterDto();
        counterDto.setCount(counter.getCounter());
        return counterDto;
    }

    @Override
    public List<CounterDto> getCounters() {
        List<Counter> counters = counterRepo.findAll();
        List<CounterDto> counterDtos = new ArrayList<>();
        for (Counter counter : counters) {
            CounterDto counterDto = new CounterDto();
            counterDto.setCount(counter.getCounter());
            counterDtos.add(counterDto);
        }
        return counterDtos;
    }
}
