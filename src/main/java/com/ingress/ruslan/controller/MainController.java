package com.ingress.ruslan.controller;

import com.ingress.ruslan.domain.Counter;
import com.ingress.ruslan.dto.CounterDto;
import com.ingress.ruslan.dto.ResponseDto;
import com.ingress.ruslan.service.CounterService;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/hello")
public class MainController {


    private final CounterService counterService;

    @GetMapping("/{id}")
    public ResponseDto sayHello(@PathVariable Long id) {
        CounterDto counter = counterService.getCounter(id);
        ResponseDto resp = new ResponseDto("Hello",  counter);

        return resp;
    }

    @GetMapping()
    public List<CounterDto> allCounter() {
        return counterService.getCounters();
    }

}
