package com.ingress.ruslan.repo;

import com.ingress.ruslan.domain.Counter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CounterRepo extends JpaRepository<Counter, Long> {
}
