FROM alpine:3.11.2
RUN apk add --no-cache openjdk8
COPY /build/libs/docker_task-1.0-SNAPSHOT.jar /app/
WORKDIR /app/
ENTRYPOINT ["java"]
CMD ["-jar","/app/docker_task-1.0-SNAPSHOT.jar"]